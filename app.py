import psycopg2

from flask import Flask, jsonify, request, make_response, render_template

app = Flask(__name__)

def get_db_connection():
    conn = psycopg2.connect(host='localhost',
                            database='py',
                            port='5432',
                            user='postgres',
                            password='admin')
    return conn

@app.route('/', methods=['GET']) 
def home(): 
    if(request.method == 'GET'): 
        data = {"data": "Hello World"} 
        return jsonify(data) 
    
@app.route('/satpolpp', methods=['GET'])
def satpolpp():
    conn = get_db_connection()
    cur = conn.cursor()

    cur.execute('select personil, tahun, jumlah_kendaraan from satpolpp')
    # row_headers = [x[0] for x in cur.description]
    result = cur.fetchall()

    json_data = []
    for result in result:
        data = {}

        data['personil'] = result[0]
        data['tahun'] = result[1]
        data['jumlah_kendaraan'] = result[2]

        json_data.append(data)

    cur.close()
    conn.close()

    return make_response(jsonify({'data':json_data,'message':'success'}),200)

@app.route('/bps', methods=['GET'])
def bps():
    conn = get_db_connection()
    cur = conn.cursor()

    cur.execute('select kode_provinsi,indeks_keparahan_kemiskinan,tahun,nama_provinsi from bps')
    # row_headers = [x[0] for x in cur.description]
    result = cur.fetchall()

    json_data = []
    for result in result:
        data = {}
        data['kode_provinsi'] = result[0]
        data['indeks_keparahan_kemiskinan'] = result[1]
        data['tahun'] = result[2]
        data['nama_provinsi'] = result[3]

        json_data.append(data)

    cur.close()
    conn.close()

    bearer = request.headers.get('Authorization')
    bearer_token = bearer.split()

    if len(bearer_token) != 2:
        return make_response(jsonify({'message':'Unauthorized header Authorization'}), 401)
    else:
        return make_response(jsonify({'data':json_data,'message':'success'}),200)

@app.route('/kesakitan_pemuda', methods=['GET'])
def pemuda():
    conn = get_db_connection()
    cur = conn.cursor()

    cur.execute('select k.nama_kabupaten_kota, k.kode_provinsi, k.tahun, sum(k.angka_kesakitan) as angka_kesakitan from kesakitan k group by k.nama_kabupaten_kota, k.id, k.kode_provinsi, k.tahun order by k.id ')
    result = cur.fetchall()

    json_data = []
    for result in result:
        data = {}
        data['nama_kabupaten_kota'] = result[0]
        data['kode_provinsi'] = result[1]
        data['tahun'] = result[2]
        data['angka_kesakitan'] = result[3]

        json_data.append(data)

    cur.close()
    conn.close()

    bearer = request.headers.get('Authorization')
    bearer_token = bearer.split()

    if len(bearer_token) != 2:
        return make_response(jsonify({'message':'Unauthorized header Authorization'}), 401)
    else:
        return make_response(jsonify({'data':json_data,'message':'success'}),200)

if __name__ == '__main__': 
    app.run(debug=True) 


